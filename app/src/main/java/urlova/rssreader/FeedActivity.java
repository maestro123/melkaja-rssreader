package urlova.rssreader;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import urlova.rssreader.core.DataProvider;
import urlova.rssreader.core.FeedTable;

public class FeedActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks {

    private static final String PARAM_ID = "id";

    public static void open(Context context, long feedId) {
        Intent intent = new Intent(context, FeedActivity.class);
        intent.putExtra(PARAM_ID, feedId);
        context.startActivity(intent);
    }

    private static final int DATA_LOADER_ID = 0;

    private Cursor mCursor;

    private ImageView mImage;
    private TextView txtTitle;
    private TextView txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        mImage = (ImageView) findViewById(R.id.image);
        txtTitle = (TextView) findViewById(R.id.title);
        txtDescription = (TextView) findViewById(R.id.description);

        getSupportLoaderManager().initLoader(DATA_LOADER_ID, null, this);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ContentUris.withAppendedId(DataProvider.CONTENT_URI, getIntent().getLongExtra(PARAM_ID, -1)), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        mCursor = (Cursor) data;
        bindData();
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void bindData() {
        if (mCursor == null || mCursor.getCount() == 0) {
            //TODO: finish with error?
            return;
        }

        mCursor.moveToFirst();

        txtTitle.setText(mCursor.getString(FeedTable.TITLE_INDEX));
        txtDescription.setText(mCursor.getString(FeedTable.DESCRIPTION_INDEX));
        Picasso.with(this).load(mCursor.getString(FeedTable.THUMBNAIL_INDEX))
                .fit()
                .centerCrop()
                .into(mImage);
    }

}