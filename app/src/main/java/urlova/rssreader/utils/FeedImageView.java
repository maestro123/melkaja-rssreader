package urlova.rssreader.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import urlova.rssreader.R;

public class FeedImageView extends android.support.v7.widget.AppCompatImageView {

    private float mRatio = 1f;

    public FeedImageView(Context context) {
        this(context, null);
    }

    public FeedImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FeedImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FeedImageView);
            mRatio = array.getFloat(R.styleable.FeedImageView_ratio, 1f);
            array.recycle();
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mRatio != 1f) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            setMeasuredDimension(width, (int) (width * mRatio));
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    private boolean isRequestBlocked = false;

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        if (mRatio != 1f) {
            isRequestBlocked = true;
        }
        super.setImageDrawable(drawable);
        isRequestBlocked = false;
    }

    @Override
    public void requestLayout() {
        if (!isRequestBlocked) {
            super.requestLayout();
        }
    }
}