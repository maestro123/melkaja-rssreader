package urlova.rssreader;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import urlova.rssreader.core.FeedTable;

public class FeedsAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    public FeedsAdapter(Context context, Cursor c) {
        super(context, c, true);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final View view = mInflater.inflate(R.layout.feed_item_view, parent, false);
        new ViewHolder(view);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();
        holder.txtTitle.setText(cursor.getString(FeedTable.TITLE_INDEX));

        final String path = cursor.getString(FeedTable.THUMBNAIL_INDEX);
        if (!TextUtils.isEmpty(path)) {
            Picasso.with(view.getContext())
                    .load(cursor.getString(FeedTable.THUMBNAIL_INDEX))
                    .fit()
                    .centerCrop()
                    .into(holder.imgThumb);
        } else {
            Picasso.with(view.getContext()).cancelRequest(holder.imgThumb);
        }
    }

    private class ViewHolder {

        private TextView txtTitle;
        private ImageView imgThumb;

        public ViewHolder(View view) {
            txtTitle = (TextView) view.findViewById(R.id.title);
            imgThumb = (ImageView) view.findViewById(R.id.thumbnail);
            view.setTag(this);
        }

    }

}