package urlova.rssreader;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import urlova.rssreader.core.RssUrl;

public class MainActivity extends AppCompatActivity {

    public static final RssUrl[] CATEGORIES = new RssUrl[]
            {
                    new RssUrl("technology", "http://feeds.bbci.co.uk/news/" + "technology" + "/rss.xml?edition=us"),
                    new RssUrl("world", "http://feeds.bbci.co.uk/news/" + "world" + "/rss.xml?edition=us"),
                    new RssUrl("business", "http://feeds.bbci.co.uk/news/" + "business" + "/rss.xml?edition=us"),
                    new RssUrl("rssdata", "rssdata.xml", true)
            };

    private ViewPager mPager;
    private BottomNavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new RssPagesAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(3);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mNavigationView.setSelectedItemId(R.id.navigation_tech);
                        return;
                    case 1:
                        mNavigationView.setSelectedItemId(R.id.navigation_world);
                        return;
                    case 2:
                        mNavigationView.setSelectedItemId(R.id.navigation_business);
                        return;
                    case 3:
                        mNavigationView.setSelectedItemId(R.id.navigation_local);
                        return;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        mNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_tech:
                    mPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_world:
                    mPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_business:
                    mPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_local:
                    mPager.setCurrentItem(3);
                    return true;
            }
            return false;
        }

    };

    private final class RssPagesAdapter extends FragmentStatePagerAdapter {

        public RssPagesAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return RssPageFragment.Create(CATEGORIES[position]);
        }

        @Override
        public int getCount() {
            return CATEGORIES.length;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

}
