package urlova.rssreader;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import urlova.rssreader.core.DataProvider;
import urlova.rssreader.core.FeedTable;
import urlova.rssreader.core.RssUpdateLoader;
import urlova.rssreader.core.RssUrl;

public class RssPageFragment extends Fragment implements LoaderManager.LoaderCallbacks {

    public static final String ARG_URL = "argUrl";

    private static final int CURSOR_LOADER_ID = 0;
    private static final int FEEDS_LOADER_ID = 1;

    //http://feeds.bbci.co.uk/news/technology/rss.xml?edition=us

    public static RssPageFragment Create(RssUrl url) {
        RssPageFragment fragment = new RssPageFragment();
        Bundle args = new Bundle(1);
        args.putSerializable(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    private SwipeRefreshLayout mRefreshLayout;
    private ListView mListView;

    private FeedsAdapter mAdapter;

    private RssUrl mUrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = (RssUrl) getArguments().getSerializable(ARG_URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rss_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        mListView = (ListView) view.findViewById(R.id.list_view);

        mRefreshLayout.setOnRefreshListener(mRefreshListener);

        mAdapter = new FeedsAdapter(getContext(), null);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mItemClickListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(CURSOR_LOADER_ID, null, this);
        getLoaderManager().initLoader(FEEDS_LOADER_ID, null, this);
    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        switch (id) {
            case CURSOR_LOADER_ID: {
                return new CursorLoader(getContext(), DataProvider.CONTENT_URI, null,
                        FeedTable.KEY + "=?", new String[]{mUrl.getKey()}, FeedTable.PUB_DATE + " DESC");
            }
            case FEEDS_LOADER_ID: {
                mRefreshLayout.setRefreshing(true);
                return new RssUpdateLoader(getContext(), mUrl);
            }
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (loader.getId() == CURSOR_LOADER_ID) {
            mAdapter.swapCursor((Cursor) data);
        } else if (loader.getId() == FEEDS_LOADER_ID) {
            //TODO: show exception if data is instance if Exception.java
            mRefreshLayout.setRefreshing(false);
            getLoaderManager().destroyLoader(FEEDS_LOADER_ID);
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private final SwipeRefreshLayout.OnRefreshListener mRefreshListener
            = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getLoaderManager().initLoader(FEEDS_LOADER_ID, null, RssPageFragment.this);
        }
    };

    private final AdapterView.OnItemClickListener mItemClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            FeedActivity.open(view.getContext(), id);
        }
    };

}