package urlova.rssreader.core;

import android.content.ContentValues;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class RssParser extends DefaultHandler {

    private List<ContentValues> mContentValues = new ArrayList<>();

    private StringBuilder mCurrentText = new StringBuilder();

    private ContentValues mCurrentValues;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (Constants.ITEM.equals(qName)) {
            mCurrentValues = new ContentValues();
        } else if (Constants.THUMBNAIL.equals(qName)) {
            mCurrentValues.put(FeedTable.THUMBNAIL, attributes.getValue(Constants.URL));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        while ((start < length) && (ch[start] <= ' ')) {
            start++;
        }
        while ((start < length) && (ch[length - 1] <= ' ')) {
            length--;
        }
        if (start < length) {
            mCurrentText.append(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (mCurrentValues != null) {
            if (Constants.ITEM.equals(qName)) {
                onItemInserted(mCurrentValues);
                mContentValues.add(mCurrentValues);
            } else if (Constants.TITLE.equals(qName)) {
                mCurrentValues.put(FeedTable.TITLE, mCurrentText.toString());
            } else if (Constants.DESCRIPTION.equals(qName)) {
                mCurrentValues.put(FeedTable.DESCRIPTION, mCurrentText.toString());
            } else if (Constants.LINK.equals(qName)) {
                mCurrentValues.put(FeedTable.LINK, mCurrentText.toString());
            } else if (Constants.GUID.equals(qName)) {
                mCurrentValues.put(FeedTable.GUID, mCurrentText.toString());
            } else if (Constants.TITLE.equals(qName)) {
                mCurrentValues.put(FeedTable.PUB_DATE, mCurrentText.toString());
            }
        }
        mCurrentText.setLength(0);
    }

    public List<ContentValues> getContentValues() {
        return mContentValues;
    }

    protected void onItemInserted(ContentValues values) {
    }

}