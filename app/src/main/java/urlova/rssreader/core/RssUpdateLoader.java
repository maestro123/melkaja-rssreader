package urlova.rssreader.core;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

public class RssUpdateLoader extends AsyncTaskLoader<Object> {

    private RssUrl mUrl;

    public RssUpdateLoader(Context context, RssUrl url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Object loadInBackground() {
        InputStream stream = null;
        try {
            if (mUrl.isLocal()) {
                stream = getContext().getAssets().open(mUrl.getUrl());
            } else {
                URLConnection connection = new URL(mUrl.getUrl()).openConnection();
                stream = connection.getInputStream();
            }

            XMLReader reader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            RssParser parser = new RssParser() {
                @Override
                protected void onItemInserted(ContentValues values) {
                    super.onItemInserted(values);
                    values.put(FeedTable.KEY, mUrl.getKey());
                }
            };
            reader.setContentHandler(parser);
            reader.parse(new InputSource(stream));

            List<ContentValues> values = parser.getContentValues();
            if (!values.isEmpty()) {
                getContext().getContentResolver().bulkInsert(DataProvider.CONTENT_URI, values.toArray(new ContentValues[values.size()]));
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}