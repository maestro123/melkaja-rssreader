package urlova.rssreader.core;

import java.io.Serializable;

public class RssUrl implements Serializable {

    private String key;
    private String url;
    private boolean isLocal;

    public RssUrl(String key, String url) {
        this(key, url, false);
    }

    public RssUrl(String key, String url, boolean isLocal) {
        this.key = key;
        this.url = url;
        this.isLocal = isLocal;
    }

    public String getKey() {
        return key;
    }

    public String getUrl() {
        return url;
    }

    public boolean isLocal() {
        return isLocal;
    }
}