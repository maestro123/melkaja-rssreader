package urlova.rssreader.core;

/**
 * Created by artsiombarouski on 5/22/17.
 */

public class Constants {

    public static final String ITEM = "item";

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LINK = "link";
    public static final String GUID = "guid";
    public static final String PUB_DATE = "pubDate";
    public static final String THUMBNAIL = "media:thumbnail";

    public static final String URL = "url";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";

}
