package urlova.rssreader.core;

public final class FeedTable extends DatabaseHelper.BaseTable {

    public static final String NAME = "tblFeeds";

    public static final int TITLE_INDEX = ID_INDEX + 1;
    public static final String TITLE = "title";

    public static final int DESCRIPTION_INDEX = ID_INDEX + 2;
    public static final String DESCRIPTION = "description";

    public static final int LINK_INDEX = ID_INDEX + 3;
    public static final String LINK = "link";

    public static final int GUID_INDEX = ID_INDEX + 4;
    public static final String GUID = "guid";

    public static final int PUB_DATE_INDEX = ID_INDEX + 5;
    public static final String PUB_DATE = "pub_date";

    public static final int THUMBNAIL_INDEX = ID_INDEX + 6;
    public static final String THUMBNAIL = "thumbnail";

    public static final int KEY_INDEX = ID_INDEX + 7;
    public static final String KEY = "key";

    public static final String CREATE = "create table " + NAME + "("
            + ID + " integer primary key autoincrement,"
            + TITLE + " text not null,"
            + DESCRIPTION + " text,"
            + LINK + " text,"
            + GUID + " text not null unique,"
            + PUB_DATE + " text,"
            + THUMBNAIL + " text,"
            + KEY + " text"
            + ")";
}